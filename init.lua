arena_lib.register_minigame("deathrun", {
  prefix = "[dr]",
  hub_spawn_point = { x = 0, y = 0, z = 0 },
  queue_waiting_time = 20,
  show_minimap = true,
  properties = {
    spawn = {0, 0, 0},
    checkpoint1 = {0, 0, 0},
    checkpoint = spawn
  },
  disabled_damage_types = {"fall"},
  hotbar = {
  },

})


if not minetest.get_modpath("lib_chatcmdbuilder") then
    dofile(minetest.get_modpath("deathrun") .. "/chatcmdbuilder.lua")
end
dofile(minetest.get_modpath("deathrun") .. "/checkpoints.lua")
dofile(minetest.get_modpath("deathrun") .. "/auto.lua")


ChatCmdBuilder.new("deathrun", function(cmd) -- In music is a music Cusade by KevinMacLeod which should using for one map

  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "deathrun", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "deathrun", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "deathrun", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "deathrun")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "deathrun", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "deathrun", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "deathrun", arena)
  end)

end, {
  description = [[

    (/help deathrun)

    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>

    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
})
