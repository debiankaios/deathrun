arena_lib.on_load("deathrun", function(arena)
  local checkpoint = arena.spawn
end)

minetest.register_on_respawnplayer(function(player, pos)
  if not arena_lib.is_player_in_arena(player:get_player_name(), "deathrun") then return end
  player:set_pos(checkpoint)
end)
