checkpoint = {}
local checkpoint

minetest.register_node("deathrun:checkpoint1", {
    description = "Checkpoint 1",
    tiles = {
        "checkpoint_1.png",
    },
    groups = {cracky = 3},
    drawtype = "glasslike_framed_optional",
    walkable = false,
    sunlight_propagates = true,
    drop = "deathrun:checkpoint1",
    node_box = {
	     type = "fixed",
	      fixed = {
		        {-0.5000, -0.5000, -0.06250, 0.5000, 0.5000, 0.06250}
	         }
    },
    paramtype = "light",
    paramtype2 = "facedir",
    drawtype = "nodebox",
})


minetest.register_abm({
	nodenames = {"deathrun:checkpoint1"},
	interval = 1,
	chance = 1,
	action = function(pos, node, arena)
		for _,object in ipairs(minetest.get_objects_inside_radius(pos, 1)) do
			if object:is_player() then
				checkpoint = checkpoint1
			end

		end
	end})
